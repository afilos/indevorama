using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Common;
using Indevorama.Common.Abstractions;
using Indevorama.Common.Dtos.Resource;
using Workloud.Common.Client;
using Workloud.Common.Client.Http;
using Workloud.Common.Core.Serializers;

namespace Indevorama.Client
{
    public class IndevoramaServiceClient : BaseServiceClient, IResourceService
    {
        public IndevoramaServiceClient(
            Uri baseAddress,
            IClientContextAccessor contextAccessor,
            IHttpClient client,
            IStringSerializer serializer,
            IHttpUtility httpUtility)
            : base(baseAddress, contextAccessor, client, serializer, httpUtility)
        {
        }

        public Task<ResourceDto> CreateAsync(CreateResourceRequest request, CancellationToken token = default)
        {
            return MakePostRequest<CreateResourceRequest, ResourceDto>(
                RouteConstants.CREATE_RESOURCE,
                request,
                cancellationToken: token);
        }

        public Task<ResourceDto> DeleteAsync(Guid id, CancellationToken token = default)
        {
            var route = string.Format(RouteConstants.DELETE_RESOURCE, id);
            return MakeRequest<ResourceDto>(
                route,
                HttpMethod.Delete,
                cancellationToken: token);
        }

        public Task<IEnumerable<ResourceDto>> GetAsync(GetResourcesRequest request, CancellationToken token = default)
        {
            var parameters = new Dictionary<string, IEnumerable<string>>();

            if (request.Name != null)
            {
                parameters[nameof(request.Name)] = new string[]
                {
                    request.Name
                };
            }

            return MakeGetRequest<IEnumerable<ResourceDto>>(
                RouteConstants.GET_RESOURCES,
                parameters: parameters,
                cancellationToken: token);
        }

        public Task<ResourceDto> GetAsync(Guid id, CancellationToken token = default)
        {
            var route = string.Format(RouteConstants.GET_RESOURCE, id);
            return MakeGetRequest<ResourceDto>(
                route,
                cancellationToken: token);
        }

        public Task<ResourceDto> UpdateAsync(Guid id, UpdateResourceRequest request, CancellationToken token = default)
        {
            var route = string.Format(RouteConstants.UPDATE_RESOURCE, id);
            return MakeRequest<UpdateResourceRequest, ResourceDto>(
                route,
                request,
                HttpMethod.Put,
                cancellationToken: token);
        }
    }
}
