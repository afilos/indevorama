using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Common.Dtos.Resource;
using Workloud.Common.Client;
using Workloud.Common.Client.Http;
using Workloud.Common.Core.Contexts;
using Workloud.Common.Core.Serializers;
using Xunit;

namespace Indevorama.Client.Tests
{
    public class ResourceServiceClientTests
    {
        private readonly WorkloudRequestContext _requestContext;
        private readonly Uri _baseAddress;

        private readonly Mock<IClientContextAccessor> _contextAccessor;
        private readonly Mock<IHttpClient> _httpClient;
        private readonly Mock<IStringSerializer> _serializer;
        private readonly Mock<IHttpUtility> _httpUtility;

        private readonly IndevoramaServiceClient _client;
        private readonly HttpResponseMessage _response;

        public ResourceServiceClientTests()
        {
            _requestContext = new WorkloudRequestContext(Guid.NewGuid(), Guid.NewGuid(), "unknown", "unknown", "token");
            _baseAddress = new Uri("http://localhost");
            _contextAccessor = new Mock<IClientContextAccessor>();
            _httpClient = new Mock<IHttpClient>();
            _serializer = new Mock<IStringSerializer>();
            _httpUtility = new Mock<IHttpUtility>();

            var expectedResult = "result";
            var content = new StringContent(expectedResult, Encoding.UTF8, "application/json");

            _response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = content
            };

            _contextAccessor
                .Setup(x => x.GetContext())
                .Returns(_requestContext);

            _httpUtility
                .Setup(x => x.ParseQueryString(It.IsAny<string>()))
                .Returns(new HttpValueCollection());

            _client = new IndevoramaServiceClient(_baseAddress, _contextAccessor.Object, _httpClient.Object, _serializer.Object, _httpUtility.Object);
        }

        [Fact]
        public async Task GetAsync_ShouldCallSendAsync_WithTheRightParameters()
        {
            var request = new GetResourcesRequest
            {
                Name = "Name",
            };

            var dtos = new[] { new ResourceDto() };

            var expected = new Dictionary<string, IEnumerable<string>>()
            {
                { nameof(request.Name), new []{ request.Name } }
            };

            _httpClient.Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_response);

            _serializer.Setup(s => s.Deserialize<IEnumerable<ResourceDto>>(It.IsAny<Stream>()))
                .Returns(dtos);

            var result = await _client.GetAsync(request, CancellationToken.None);

            _httpClient.Verify(x =>
               x.SendAsync(
                   It.Is<HttpRequestMessage>(r =>
                       r.Method == HttpMethod.Get
                       && ParametersExistInUri(r.RequestUri, expected)),
                   CancellationToken.None), Times.Once);

            result.Should().BeEquivalentTo(dtos);
        }

        public bool ParametersExistInUri(Uri uri, Dictionary<string, IEnumerable<string>> expectedKeyValuePairs)
        {
            var query = uri.Query;
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(query);

            var result = true;

            foreach (var parameter in expectedKeyValuePairs)
            {
                var split = queryDictionary[parameter.Key].Split(",");

                foreach (var value in split)
                {
                    if (parameter.Value.Any(x => x == value))
                        result = true;
                    else
                        result = false;
                }
            }

            return result;
        }
    }
}
