using FluentAssertions;
using System;
using Indevorama.Core.DataAccess;
using Indevorama.Core.Mappers;
using Xunit;

namespace Indevorama.Core.Tests.Mappers
{
    public class ResourceMapperTests
    {
        private readonly ResourceMapper _mapper;

        public ResourceMapperTests()
        {
            _mapper = new ResourceMapper();
        }

        [Fact]
        public void Map_ShouldReturnNull_WhenSourceIsDefault()
        {
            var result = _mapper.Map(default(Resource));
            result.Should().BeNull();
        }

        [Fact]
        public void Map_ShouldReturnDto_WhenSourceIsNotNull()
        {
            var resource = new Resource
            {
                Id = Guid.NewGuid(),
                Name = "name"
            };

            var result = _mapper.Map(resource);
            result.Should().NotBeNull();
            result.Id.Should().Be(resource.Id);
            result.Name.Should().Be(resource.Name);
        }
    }
}
