using FluentAssertions;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Common.Dtos.Resource;
using Indevorama.Core.DataAccess;
using Indevorama.Core.Services;
using Workloud.Common.Core.Mappers;
using Workloud.Common.Persistence;
using Xunit;

namespace Indevorama.Core.Tests.Services
{
    public class ResourceServiceTests
    {
        private readonly ResourceService _service;
        private readonly Mock<IMapper<Resource, ResourceDto>> _mapper;
        private readonly Mock<IRepository<Resource, Guid>> _repository;

        private readonly Resource _resource;
        private readonly ResourceDto _resourceDto;

        public ResourceServiceTests()
        {
            _mapper = new Mock<IMapper<Resource, ResourceDto>>();
            _repository = new Mock<IRepository<Resource, Guid>>();

            _resource = new Resource
            {
                Id = Guid.NewGuid(),
                Name = "name"
            };
            _resourceDto = new ResourceDto
            {
                Id = _resource.Id,
                Name = _resource.Name
            };

            _service = new ResourceService(_mapper.Object, _repository.Object);

            _mapper.Setup(m => m.Map(_resource))
                .Returns(_resourceDto);
            _repository.Setup(r => r.CreateAsync(It.IsAny<Resource>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_resource);
        }

        [Fact]
        public async Task CreateAsync_ShouldReturnDto()
        {
            var request = new CreateResourceRequest
            {
                Name = "name"
            };

            var result = await _service.CreateAsync(request, CancellationToken.None);

            result.Should().Be(_resourceDto);

            _mapper.Verify(m => m.Map(_resource), Times.Once());
            _repository.Verify(r => r.CreateAsync(It.IsAny<Resource>(), It.IsAny<CancellationToken>()), Times.Once());
        }
    }
}
