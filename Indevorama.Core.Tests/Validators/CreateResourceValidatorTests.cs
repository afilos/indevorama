using FluentAssertions;
using Indevorama.Common.Dtos.Resource;
using Indevorama.Core.Validators;
using Xunit;
using FluentValidation.TestHelper;

namespace Indevorama.Core.Tests.Validators
{
    public class CreateResourceValidatorTests
    {
        private readonly CreateResourceValidator _validator;

        public CreateResourceValidatorTests()
        {
            _validator = new CreateResourceValidator();
        }

        [Fact]
        public void Validate_ShouldReturnTrue_WhenRequestHasName()
        {
            var request = new CreateResourceRequest
            {
                Name = "name"
            };

            var result = _validator.Validate(request);

            result.IsValid.Should().Be(true);
        }

        [Fact]
        public void Validate_ShouldReturnFalse_WhenRequestHasEmptyName()
        {
            var request = new CreateResourceRequest
            {
                Name = ""
            };

            var result = _validator.Validate(request);

            result.IsValid.Should().Be(false);
        }

        [Fact]
        public void Validate_ShouldNotValidate_NullName()
        {
            _validator.ShouldHaveValidationErrorFor(r => r.Name, default(string));
        }

        [Fact]
        public void Validate_ShouldValidate_NonEmptyName()
        {
            _validator.ShouldNotHaveValidationErrorFor(r => r.Name, "test");
        }
    }
}
