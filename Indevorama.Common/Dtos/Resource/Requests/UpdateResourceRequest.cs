namespace Indevorama.Common.Dtos.Resource
{
    public class UpdateResourceRequest
    {
        public string Name { get; set; }
    }
}
