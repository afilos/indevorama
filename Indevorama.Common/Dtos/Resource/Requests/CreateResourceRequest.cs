namespace Indevorama.Common.Dtos.Resource
{
    public class CreateResourceRequest
    {
        public string Name { get; set; }
    }
}
