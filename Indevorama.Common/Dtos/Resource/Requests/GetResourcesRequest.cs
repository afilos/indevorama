namespace Indevorama.Common.Dtos.Resource
{
    public class GetResourcesRequest
    {
        public string Name { get; set; }
    }
}
