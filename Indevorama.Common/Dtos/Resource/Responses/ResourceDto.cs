using System;

namespace Indevorama.Common.Dtos.Resource
{
    public class ResourceDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
