namespace Indevorama.Common
{
	public static class RouteConstants
    {
        public const string RESOURCES_BASE = "api/resources";
        public const string CREATE_RESOURCE = RESOURCES_BASE;
        public const string GET_RESOURCES = RESOURCES_BASE;
        public const string GET_RESOURCE = RESOURCES_BASE + "/{0}";
        public const string UPDATE_RESOURCE = RESOURCES_BASE + "/{0}";
        public const string DELETE_RESOURCE = RESOURCES_BASE + "/{0}";
    }
}
