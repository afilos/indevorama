using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Common.Dtos.Resource;

namespace Indevorama.Common.Abstractions
{
    public interface IResourceService
    {
        Task<IEnumerable<ResourceDto>> GetAsync(GetResourcesRequest request, CancellationToken token = default);
        Task<ResourceDto> GetAsync(Guid id, CancellationToken token = default);
        Task<ResourceDto> CreateAsync(CreateResourceRequest request, CancellationToken token = default);
        Task<ResourceDto> UpdateAsync(Guid id, UpdateResourceRequest request, CancellationToken token = default);
        Task<ResourceDto> DeleteAsync(Guid id, CancellationToken token = default);
    }
}
