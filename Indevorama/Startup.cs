using System;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Converters;
using Serilog;
using Workloud.Common.Core.Config;
using Workloud.Common.Web.ModelBinders;

namespace Indevorama
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method To add services To the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var workloudAuthenticationSection = Configuration.GetSection("WorkloudAuthentication");

            services.TryAddInsights(Configuration);
            services.AddWorkloudLogging(Configuration);
            services.AddDefaultHealthChecks();
            services.AddDefaultMemoryDistributedCaching();
            services.AddBearerTokenAuthentication(options => workloudAuthenticationSection.Bind(options));
            services.AddWorkloudAuthorization(new Uri(Configuration.GetSection("WorkloudAuthorization")["Endpoint"]));

            var sqlConnectionString = Configuration.GetConnectionString("SqlServer");

            services.InitPersistenceSubsystem(sqlConnectionString);
            services.InitLogicSubsystem();
            services.InitDocumentationSubsystem();
            services.AddCors(options => { options.AddPolicy("ApiName", policy => { policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); }); });

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services
                .AddWorkloudWebControllers(options => options.ModelBinderProviders.Insert(0, new WorkloudBinderProvider()))
                .AddNewtonsoftJson(options => options.SerializerSettings.Converters.Add(new StringEnumConverter()))
                .AddFluentValidation();

            services.Configure<BaseServiceConfig>(options =>
            {
                options.Version = Configuration.GetSection("ServiceInformation")["Version"];
            });

            // PLACEHOLDER for DEVELOPMENT
            //if (Environment.IsDevelopment())
            //{
            //    services.AddScoped<IWorkloudRequestContextAccessor, FakeWorkloudRequestContextAccessor>();
            //}
        }

        // This method gets called by the runtime. Use this method To configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var serviceConfig = app.ApplicationServices.GetService<IOptions<BaseServiceConfig>>().Value;

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .Enrich.WithProperty("ApiName", serviceConfig.Service)
                .Enrich.WithProperty("ApiVersion", serviceConfig.Version)
                .CreateLogger();

            app
                .UseAuthentication()
                .UseRouting()
                .EnableCors()
                .UseAuthorization()
                .UseDefaultMetrics()
                .EnableCultureSupport()
                .EnableDocumentation()
                .UseEndpoints(config =>
                 {
                     config.MapDefaultControllerRoute();
                     config.MapDefaultHealthChecks();
                 });
        }
    }
}