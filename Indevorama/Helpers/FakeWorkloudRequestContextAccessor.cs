using System;
using System.Collections.Generic;
using Workloud.Common.Core.Contexts;

namespace Indevorama
{
	public class FakeWorkloudRequestContextAccessor : IWorkloudRequestContextAccessor
	{
		private readonly Guid _user;
		private readonly Guid _domain;
		private readonly Guid _ou;

		public FakeWorkloudRequestContextAccessor(string user = null, string domain = null, Guid? ou = null)
		{
			//domainadmin@Training Template QA 6065A0F5-A9CC-442D-8652-3BDA47CA2E6E
			_user = !string.IsNullOrEmpty(user)
				? Guid.Parse(user)
				: Guid.Parse("6065A0F5-A9CC-442D-8652-3BDA47CA2E6E");
			//Training Template QA DE445F32-69B2-4DFB-9ED6-D199F566854A
			_domain = !string.IsNullOrEmpty(domain)
				? Guid.Parse(domain)
				: Guid.Parse("017CF490-6300-4683-8BDA-0F654F053164");

			/* Training Template QA
             4F15165D-88C9-4422-9FCC-0F7C30246ABB Company X
            EE4715EF-B007-4367-AF1E-2DF076F4AFFE Department 1
            FCD6CB8D-DB9D-4799-BF11-43D9E4F69B0F Facility A
             */
			_ou = ou ?? Guid.Parse("9885FCA3-63D9-440D-A280-C00672ED8CF5");
		}
		// Todo This is for testing purposes
		public WorkloudRequestContext GetContext()
		{
			return new WorkloudRequestContext(
				_user,
				_domain,
				"originAppiD",
				"originAppVersion",
				"accessToken",
				_ou,
				new List<string>(),
				new List<KeyValuePair<string, IEnumerable<string>>>()
				);
		}
	}
}
