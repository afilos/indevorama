using Microsoft.AspNetCore.Localization;
using System;
using System.Globalization;

namespace Microsoft.AspNetCore.Builder
{
    public static class ApiBuilderExtensions
    {
        public static IApplicationBuilder EnableDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Indevorama V1"); });

            return app;
        }

        public static IApplicationBuilder EnableCultureSupport(this IApplicationBuilder app)
        {
            var enUsCulture = new CultureInfo("en-US");
            var supportedCultures = new[]
            {
                enUsCulture,
                new CultureInfo("el-GR"),
                new CultureInfo("fr")
            };
            var options = new RequestLocalizationOptions()
            {
                DefaultRequestCulture = new RequestCulture(enUsCulture),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            };
            options.RequestCultureProviders = new IRequestCultureProvider[] {new AcceptLanguageHeaderRequestCultureProvider {Options = options}};
            app.UseRequestLocalization(options);

            return app;
        }

        public static IApplicationBuilder EnableCors(this IApplicationBuilder app)
        {
            app.UseCors(
                builder => builder
                    .AllowCredentials()
                    .SetPreflightMaxAge(TimeSpan.FromSeconds(2520)));

            return app;
        }
    }
}