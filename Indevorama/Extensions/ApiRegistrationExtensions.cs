using System;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Indevorama.Common.Abstractions;
using Indevorama.Common.Dtos.Resource;
using Indevorama.Core.DataAccess;
using Indevorama.Core.Mappers;
using Indevorama.Core.Repositories;
using Indevorama.Core.Services;
using Indevorama.Core.Validators;
using Workloud.Common.Core.Mappers;
using Workloud.Common.Persistence;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ApiRegistrationExtensions
    {
        public static IServiceCollection InitPersistenceSubsystem(this IServiceCollection services, string connectionString)
        {
            //Repositories
            services.AddScoped<IRepository<Resource, Guid>, IndevoramaRepository>();

            return services;
        }

        public static IServiceCollection InitLogicSubsystem(this IServiceCollection services)
        {
            //Services
            services.AddScoped<IResourceService, ResourceService>();

            //Helpers

            //Validators
            services.AddTransient<IValidator<CreateResourceRequest>, CreateResourceValidator>();

            //Mappers 
            services.AddScoped<IMapper<Resource, ResourceDto>, ResourceMapper>();

            return services;
        }

        public static IServiceCollection InitDocumentationSubsystem(this IServiceCollection services)
        {
            return services.AddSwaggerGen(
                c =>
                {
                    c.AddSecurityDefinition(
                        "token",
                        new OpenApiSecurityScheme
                        {
                            Description = "JWT based auth",
                            Name = "Authorization",
                            In = ParameterLocation.Header,
                            Type = SecuritySchemeType.ApiKey
                        });
                    c.SwaggerDoc(
                        "v1",
                        new OpenApiInfo
                        {
                            Title = "Indevorama",
                            Version = "v1"
                        });
                });
        }

        public static IServiceCollection TryAddInsights(this IServiceCollection services, IConfiguration configuration)
        {
            var key = configuration
                .GetSection("ApiApplicationInsights")
                .GetValue<string>("InstrumentationKey");

            return string.IsNullOrEmpty(key)
                ? services
                : services.AddApplicationInsightsTelemetry(key);
        }
    }
}