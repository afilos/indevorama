using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Common.Abstractions;
using Indevorama.Common.Dtos.Resource;
using Workloud.Common.Core.Factories;
using Workloud.Common.Core.Responses;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Indevorama.Controllers
{
	[Route("api/resources")]
	[Consumes("application/json")]
	[Produces("application/json")]
	[ProducesResponseType(typeof(ErrorReport), Status400BadRequest)]
	[ProducesResponseType(typeof(ErrorReport), Status401Unauthorized)]
	[ProducesResponseType(typeof(ErrorReport), Status403Forbidden)]
	[ProducesResponseType(typeof(ErrorReport), Status500InternalServerError)]
	public class ResourcesController : Controller
	{
		private readonly IResourceService _service;
		private readonly IErrorFactory _errorFactory;

		public ResourcesController(IResourceService service, IErrorFactory errorFactory)
		{
			_service = service ?? throw new ArgumentNullException(nameof(service));
			_errorFactory = errorFactory ?? throw new ArgumentNullException(nameof(errorFactory));
		}

		/// <summary>
		/// Get resources.
		/// </summary>
		[HttpGet]
		[ProducesResponseType(typeof(IEnumerable<ResourceDto>), Status200OK)]
		public async Task<IActionResult> GetResourcesAsync([FromQuery] GetResourcesRequest request, CancellationToken token = default)
		{
			var result = await _service.GetAsync(request, token);
			return Ok(result);
		}

		/// <summary>
		/// Get resource.
		/// </summary>
		[HttpGet("{id}")]
		[ProducesResponseType(typeof(ResourceDto), Status200OK)]
		[ProducesResponseType(typeof(ErrorReport), Status404NotFound)]
		public async Task<IActionResult> GetAsync([FromRoute] Guid id, CancellationToken token = default)
		{
			var result = await _service.GetAsync(id, token);
			return Ok(result);
		}

        /// <summary>
        /// Creates a resource.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(ResourceDto), Status201Created)]
        public async Task<IActionResult> CreateAsync([FromBody] CreateResourceRequest request, CancellationToken token = default)
        {
            var result = await _service.CreateAsync(request, token);
            return CreatedAtAction(nameof(GetAsync), new { result.Id }, result);
        }

        ///// <summary>
        ///// Updates a resource.
        ///// </summary>
        //[HttpPut("{id}")]
        //public async Task<IActionResult> UpdateAsync([FromRoute] Guid id, [FromBody]UpdateResourceRequest request, CancellationToken token = default)
        //{
        //	var result = await _service.UpdateAsync(id, request, token);
        //	return Ok(result);
        //}

        ///// <summary>
        ///// Deletes a specific resource.
        ///// </summary>
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteAsync([FromRoute]Guid id, CancellationToken token = default)
        //{
        //	var result = await _service.DeleteAsync(id, token);
        //	return Ok(result);
        //}
    }
}
