using System;

namespace Indevorama.Core.DataAccess
{
    public class Resource
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
