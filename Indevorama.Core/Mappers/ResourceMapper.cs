using Indevorama.Common.Dtos.Resource;
using Indevorama.Core.DataAccess;
using Workloud.Common.Core.Mappers;

namespace Indevorama.Core.Mappers
{
    public class ResourceMapper : MapperBase<Resource, ResourceDto>
    {
        public override ResourceDto Map(Resource source)
        {
            if (source == null) return null;

            var result = new ResourceDto
            {
                Id = source.Id,
                Name = source.Name
            };

            return result;
        }
    }
}
