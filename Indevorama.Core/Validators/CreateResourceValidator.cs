using FluentValidation;
using Indevorama.Common.Dtos.Resource;

namespace Indevorama.Core.Validators
{
    public class CreateResourceValidator : AbstractValidator<CreateResourceRequest>
    {
        public CreateResourceValidator()
        {
            RuleFor(r => r.Name).NotEmpty();
        }
    }
}
