using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Common.Abstractions;
using Indevorama.Common.Dtos.Resource;
using Indevorama.Core.DataAccess;
using Workloud.Common.Core.Mappers;
using Workloud.Common.Persistence;

namespace Indevorama.Core.Services
{
    public class ResourceService : IResourceService
    {
        private readonly IMapper<Resource, ResourceDto> _mapper;
        private readonly IRepository<Resource, Guid> _repository;

        public ResourceService(IMapper<Resource, ResourceDto> mapper, IRepository<Resource, Guid> repository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<ResourceDto> CreateAsync(CreateResourceRequest request, CancellationToken token = default)
        {
            var resource = new Resource
            {
                Id = Guid.NewGuid(),
                Name = request.Name
            };

            resource = await _repository.CreateAsync(resource, token);

            return _mapper.Map(resource);
        }

        public async Task<ResourceDto> DeleteAsync(Guid id, CancellationToken token = default)
        {
            var resource = await _repository.RemoveAsync(id, token);

            return _mapper.Map(resource);
        }

        public async Task<IEnumerable<ResourceDto>> GetAsync(GetResourcesRequest request, CancellationToken token = default)
        {
            var resources = await _repository.SearchAsync(r => request.Name == null || r.Name == request.Name);

            return _mapper.Map(resources);
        }

        public async Task<ResourceDto> GetAsync(Guid id, CancellationToken token = default)
        {
            var resource = await _repository.GetAsync(id, token);

            return _mapper.Map(resource);
        }

        public async Task<ResourceDto> UpdateAsync(Guid id, UpdateResourceRequest request, CancellationToken token = default)
        {
            var resource = new Resource
            {
                Id = id,
                Name = request.Name
            };

            resource = await _repository.UpdateAsync(id, resource, token);

            return _mapper.Map(resource);
        }
    }
}
