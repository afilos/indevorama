using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Indevorama.Core.DataAccess;
using Workloud.Common.Persistence;

namespace Indevorama.Core.Repositories
{
    public class IndevoramaRepository : IRepository<Resource, Guid>
    {
        private readonly IDictionary<Guid, Resource> _cache;

        public IndevoramaRepository()
        {
            _cache = new ConcurrentDictionary<Guid, Resource>();
        }

        public IQueryable<Resource> AsQueryable()
        {
            return _cache.Values.AsQueryable();
        }

        public Task<Resource> CreateAsync(Resource entity, CancellationToken token = default)
        {
            _cache[entity.Id] = entity;
            return Task.FromResult(entity);
        }

        public async Task<IEnumerable<Resource>> CreateAsync(IEnumerable<Resource> entities, CancellationToken token = default)
        {
            var tasks = entities
                .Select(async e => await CreateAsync(e));
            return await Task.WhenAll(tasks);
        }

        public Task<IEnumerable<Resource>> GetAllAsync(CancellationToken token = default)
        {
            return Task.FromResult(_cache.Values.AsEnumerable());
        }

        public Task<Resource> GetAsync(Guid id, CancellationToken token = default)
        {
            _cache.TryGetValue(id, out var resource);
            return Task.FromResult(resource);
        }

        public Task<Resource> RemoveAsync(Guid id, CancellationToken token = default)
        {
            if (_cache.TryGetValue(id, out var resource))
            {
                _cache.Remove(id);
                return Task.FromResult(resource);
            }
            return Task.FromResult(default(Resource));
        }

        public Task<IEnumerable<Resource>> SearchAsync(Expression<Func<Resource, bool>> options, CancellationToken token = default)
        {
            return Task.FromResult(_cache.Values.Where(options.Compile()));
        }

        public Task<Resource> UpdateAsync(Guid id, Resource entity, CancellationToken token = default)
        {
            _cache[id] = entity;
            return Task.FromResult(entity);
        }

        public Task<Resource> UpdateAsync(Guid id, Resource entity, bool createIfNotFound, CancellationToken token = default)
        {
            _cache[id] = entity;
            return Task.FromResult(entity);
        }

        public async Task<IEnumerable<Resource>> UpdateAsync(IDictionary<Guid, Resource> entities, CancellationToken token = default)
        {
            var tasks = entities
                .Select(async pair => await UpdateAsync(pair.Key, pair.Value));
            return await Task.WhenAll(tasks);
        }
    }
}
