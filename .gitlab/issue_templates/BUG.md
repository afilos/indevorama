# Summary

(Add a high level summary of the bug)

## Test that exposes the bug

(Add merge request including test/tests that expose the bug)

## Steps to reproduce

(How one can reproduce this issue? This is very important)

1.  
2.  
3.  

## Environment
  
(Add details about the environment that this bug can be reproduced)

## What is the current bug behavior

(What actually happens)

## What is the expected correct behavior

(What you should be happening instead)

## Bug ticket

(Add bug ticket here)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem) This is useful but optional.
