# Please check if the merge request fulfills these requirements

* [ ] I have read and followed our code principles
* [ ] Tests for the changes have been added (for bug fixes/features)
* [ ] Docs have been added / updated
* [ ] ChangeLog has been updated
* [ ] The appropriate merge label has been entered

**Also check if true**:

* [ ] I have run my changes and everything is working as expected
* [ ] I have run my changes in **Docker**
* [ ] All the tests are passing

## What is the current behavior

 (You can also link to an open issue here, like a related proposal etc.)

## What is new behavior introduced  

 (Describe which is the new behaviour that this merge request introduces)

## What is the actual need under this new feature  

 (This can help us to track down the decisions that we took in order to develop this)  

* example: Users are not able to delete a draft after creating a mistaken one.  

## Describe alternatives you've considered  

(A clear and concise description of any alternative solutions or features you've considered)

## Does this merge request introduce a breaking change

(What changes might users need to make in their application due to this?)

## Ticket Url

## Other information
  