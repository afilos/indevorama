# Indevorama presentation API Releases

- [Indevorama presentation API Releases](#indevorama-presentation-api-releases)
  - [1.0.3-hotfix.1](#103-hotfix1)
  - [1.0.3](#103)
  - [1.0.2](#102)
  - [1.0.1](#101)
  - [1.0.0](#100)

## 1.0.3-hotfix.1

- Fix wrong controller HTTP verb.

## 1.0.3

- Add create controller method for resource.

## 1.0.2

- Minor configuration changes.

## 1.0.1

- Rename service client.
- Add surname property to resource data access object.
- Rename `ResourceRepository` to `IndevoramaRepository`.

## 1.0.0

- Initial release.
